module DataHelper

import Statistics

struct StateRange
	min::Number
	max::Number
end

function Base.in(x::Number, sr::StateRange)
	x <= sr.max && x >= sr.min
end

function Base.length(sr::StateRange)
	sr.max - sr.min
end

"""
    expand(sr::StateRange, kind::Symbol)

Create a range next to the `sr` range with same length.

...
# Arguments
- `sr::StateRange`: the range to be extended.
- `kind::Symbol`: except `:up` and `:down`
"""
expand(sr::StateRange, kind::Symbol) = expand(sr, Val(kind))
function expand(sr::StateRange, ::Val{:up})
	StateRange(sr.max, sr.max + length(sr))
end
function expand(sr::StateRange, ::Val{:down})
	StateRange(sr.min - length(sr), sr.min)
end


"""
	expand(sr::StateRange,point::Number,dir::Symbol)

Expand a `StateRange` in a direction until it contains the point

...
# Arguments
- `sr::StateRange`:
- `point::Number`:
- `dir::Symbol`:
"""
function expand(sr::StateRange, point::Number, dir::Symbol)
    slen = length(sr)
    # find the number if states to expand
    nstate::Int = if dir == :up
        (point - sr.max) ÷ slen + 1
    elseif dir == :down
        (sr.min - point) ÷ slen + 1
    else
        @error "invalid direction $(dir)"
    end
    # the nstate is ensured to be greater than zero
	 if nstate <= 0 @error "Number of new state is less than zero, maybe the direction is wrong?" end
	 # calculate new StatesRange
    states = StateRange[]
    sizehint!(states, nstate)
    push!(states, expand(sr, dir))
    for i in 2:nstate
        push!(states, expand(states[i - 1], dir))
    end
    return states
end


"""
    state_divide(data::AbstractArray,n::Integer)

Divide the data into `n` states
"""
function state_divide(data::AbstractArray, n::Integer)
	maxw = maximum(data)
	minw = minimum(data)
	range_end_point = range(minw, maxw; length = n + 1);
	ranges = map(Pair.(range_end_point[1:(end - 1)], range_end_point[2:end])) do pair
		StateRange(pair[1], pair[2])
	end
	labels = map(data) do datum
		reduce(max, map(Iterators.enumerate(ranges)) do (i, range); datum in range ? i : 0 end)
	end
	return labels, ranges
end

function state_divide(data::AbstractArray, dist::AbstractVector)
	mx = maximum(data) + 3 * eps()
	mn = minimum(data) - 3 * eps()
	range_end_point = (mx - mn) .* cf(dist) .+ mn
	ranges = map(Pair.(range_end_point[1:(end - 1)], range_end_point[2:end])) do pair
		StateRange(pair[1], pair[2])
	end
	labels = map(data) do datum
		maximum(map(Iterators.enumerate(ranges)) do (i, range)
					  if datum <= range.max || datum >= range.min
						  i
					  else
						  0
					  end
				  end)
	end
	for i in labels
		if i == 0
			@show length(ranges)
			display(ranges)
			display([labels data])
		end
	end
	return labels, ranges
end

"""
    which_state(states::AbstractArray{StateRange}, point::Number)

Find the index of the state that the data point is in

...
# Arguments
- `point::Number`: The data point
- `states::AbstractArray{StateRange}`: Array of state ranges
"""
function which_state(states::AbstractArray{StateRange}, point::Number)
	findfirst(map(states) do state; point in state end)
end

function Base.maximum(states::AbstractArray{StateRange})
	findmax(map(states) do state; state.max end)
end

function Base.minimum(states::AbstractArray{StateRange})
	findmin(map(states) do state; state.min end)
end

function Statistics.mean(sr::StateRange)
	return floor((sr.max + sr.min) * 0.5)
end

function true_positive(results, labels)
    count(@. results == labels == 1) / count(labels)
end

function false_alarm(results, labels)
    count(@. 1 == results != labels) / count(results)
end

function cf(d::AbstractVector)
	c = [0; [sum(d[1:i]) for i in eachindex(d)]]
	c ./ c[end]
end


export StateRange, state_divide, which_state, expand #=
	=# , true_positive, false_alarm
# end of module
end
