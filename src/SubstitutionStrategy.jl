module SubstitutionStrategies

using ..SlidingWindows
using ..DataHelper
using Polynomials
using Statistics

export substitute

abstract type AbstractSubstitutionStrategy end

struct MeanStateRange <: AbstractSubstitutionStrategy end
struct MeanStateRangePreRetrain <: AbstractSubstitutionStrategy end
struct TrendStateRange <: AbstractSubstitutionStrategy end
struct TrendStateRangePreRetrain <: AbstractSubstitutionStrategy end
struct MimicPrevious <: AbstractSubstitutionStrategy end
struct MimicPreviousPreRetrain <: AbstractSubstitutionStrategy end
struct CombineMimicTSR <: AbstractSubstitutionStrategy end
struct Skip <: AbstractSubstitutionStrategy end
struct LeastSquare <: AbstractSubstitutionStrategy end
struct LeastSquareCubic <: AbstractSubstitutionStrategy end

function substitute(::Type{Nothing}, wl, xt, states, oldstates, labels, st_idx, order)
	return xt
end

function substitute(T::Type{MeanStateRange}
							  , wt::AbstractArray
							  , xt::Real
							  , states::AbstractArray
							  , oldstates::AbstractArray
							  , labels::AbstractArray
							  , st_idx::Number
							  , order::Integer)
	return mean(states[st_idx])
end

function substitute(T::Type{MeanStateRangePreRetrain}
							  , wt::AbstractArray
							  , xt::Real
							  , states::AbstractArray
							  , oldstates::AbstractArray
							  , labels::AbstractArray
							  , st_idx::Number
							  , order::Integer)
	return mean(oldstates[st_idx])
end

function substitute(T::Type{TrendStateRange}
							  , wt::AbstractArray, xt::Real
							  , states::AbstractArray
							  , oldstates::AbstractArray
							  , labels::AbstractArray
							  , st_idx::Number
							  , order::Integer)
	st′ = states[st_idx]
	if wt[end] < xt
		xt = st′.min
	else
		xt = st′.max
	end
end

function substitute(T::Type{TrendStateRangePreRetrain}
							  , wt::AbstractArray, xt::Real
							  , states::AbstractArray
							  , oldstates::AbstractArray
							  , labels::AbstractArray
							  , st_idx::Number
							  , order::Integer)
	st′ = oldstates[st_idx]
	if wt[end] - wt[end - 1] < xt
		xt = st′.min + wt[end] - wt[end - 1]
	else
		xt = st′.max + wt[end] - wt[end - 1]
	end
end

function substitute(T::Type{TrendStateRangePreRetrain}
							  , wt::AbstractArray, xt::Real
							  , states::AbstractArray
							  , oldstates::AbstractArray
							  , labels::AbstractArray
							  , st_idx::Number
							  , order::Integer)
	st′ = oldstates[st_idx]
	if wt[end] - wt[end - 1] < xt
		xt = st′.min
	else
		xt = st′.max
	end
end

function substitute(T::Type{MimicPrevious}
							  , wt::AbstractArray, xt::Real
							  , states::AbstractArray
							  , oldstates::AbstractArray
							  , labels::AbstractArray
							  , st_idx::Number
							  , order::Integer)
	prev_idx = findlast(labels[end - order:end] .== st_idx)
	prev_mimic = isnothing(prev_idx) ? wt[end - 1] : wt[prev_idx]
	st′ = states[st_idx]
	if wt[end] - wt[end - 1] < xt
		xt = (st′.min + wt[end] + prev_mimic) / 3
	else
		xt = (st′.max + wt[end] + prev_mimic) / 3
	end
end

function substitute(T::Type{MimicPreviousPreRetrain}
							  , wt::AbstractArray, xt::Real
							  , states::AbstractArray
							  , oldstates::AbstractArray
							  , labels::AbstractArray
							  , st_idx::Number
							  , order::Integer)
	prev_idx = findlast(labels[end - order:end] .== st_idx)
	prev_mimic = isnothing(prev_idx) ? wt[end - 1] : wt[prev_idx]
	st′ = oldstates[st_idx]
	if wt[end] - wt[end - 1] < xt
		xt = (st′.min + wt[end] + prev_mimic) / 3
	else
		xt = (st′.max + wt[end] + prev_mimic) / 3
	end
end

function substitute(T::Type{LeastSquare}
							  , wt::AbstractArray, xt::Real
							  , states::AbstractArray
							  , oldstates::AbstractArray
							  , labels::AbstractArray
							  , st_idx::Number
							  , order::Integer)
	lasti = lastindex(wt)
	p = Polynomials.fit(1:lastindex(wt), wt, 1)
	xt_new = p(lasti + 1)
	st′ = oldstates[st_idx]
	if xt_new in st′
		xt_new
	else
		dx = abs(xt_new - st′.max)
		dn = abs(xt_new - st′.min)
		endpoint = dx < dn ? st′.max : st′.min
		(endpoint + xt_new + wt[end]) / 3
	end
end

function substitute(T::Type{LeastSquareCubic}
							  , wt::AbstractArray, xt::Real
							  , states::AbstractArray
							  , oldstates::AbstractArray
							  , labels::AbstractArray
							  , st_idx::Number
							  , order::Integer)
	lasti = lastindex(wt)
	p = Polynomials.fit(1:lastindex(wt), wt, 3)
	xt_new = p(lasti + 1)
	st′ = oldstates[st_idx]
	if xt_new in st′
		xt_new
	else
		dx = abs(xt_new - st′.max)
		dn = abs(xt_new - st′.min)
		endpoint = dx < dn ? st′.max : st′.min
		(endpoint + xt_new + wt[end]) / 3
	end
end

end
