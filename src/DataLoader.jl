module DataLoader

using DataFrames
using CSV
using Dates

abstract type AbstractDataSet end

function data_load(datafile, labelsfile)

	# load the labels
	# converts string datetime to Dates.DateTime
	labels = begin
		txt_labels = read(labelsfile, String) |> JSON.parse
		labels = Dict()
		labels["point"] = map(txt_labels["point_label"]) do dt
			DateTime(dt, dateformat"yyyy-mm-dd HH:MM:SS.s")
		end
		labels["range"] = map(txt_labels["range_label"]) do dtrange
			@. DateTime(dtrange, dateformat"yyyy-mm-dd HH:MM:SS.s")
		end
		labels
	end

	# extract data from csv
	begin
		txt_df = CSV.File(datafile) |> DataFrame!

		# convert to Dates.DateTime
		realdates = map(txt_df.timestamp) do dt
			DateTime(dt, dateformat"yyyy-mm-dd HH:MM:SS.s")
		end

		# Vectorize the labels
		labels = map(realdates) do dt
			isanomally = dt ∈ nyc_taxi_label["point"] || 
			any(map(nyc_taxi_label["range"]) do dtrange
					 dt >= first(dtrange) &&
					 dt <= last(dtrange)
				 end)
		end

		# complete DataFrame
		DataFrame(timestamp = realdates, value = txt_df.value, label = labels)
	end
end

end
