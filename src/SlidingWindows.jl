module SlidingWindows

using Statistics

"""
    SlidingWindow(data::AbstractVector, len::Int)

Create a sliding window of length `len` within the `data`.

## Example
```julia
julia> w = SlidingWindow(1:9, 3)
SlidingWindow(1:9, 3)

julia> for wn in w
           @show wn
       end
wn = 1:3
wn = 2:4
wn = 3:5
wn = 4:6
wn = 5:7
wn = 6:8
wn = 7:9
```
"""
mutable struct SlidingWindow
    data::AbstractArray
    len::Int
end

function Base.iterate(w::SlidingWindow, state = 1)
    if state > length(w.data) - w.len + 1
        nothing
    else
        (w.data[state:(state + w.len - 1)], state + 1)
    end
end

function Base.length(w::SlidingWindow)
    return length(w.data) - w.len + 1
end

function Base.getindex(w::SlidingWindow, i::Integer)
	w.data[i:i + w.len - 1]
end

function Base.getindex(w::SlidingWindow, is::AbstractArray)
	map(is) do i; w[i] end
end

Base.firstindex(w::SlidingWindow) = 1
function Base.lastindex(w::SlidingWindow)
    return length(w)
end

export SlidingWindow
end
