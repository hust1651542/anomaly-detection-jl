function SlidingWindows.maxorder(w::SlidingWindow; mincov::Real = 0.8)
	order = 1
	for n = 1:length(w)
		for m = (n+1):length(w)
			if cov(w[n], w[m]) >= mincov
				order = max(record, m - n)
			end
		end
	end
	return order
end

