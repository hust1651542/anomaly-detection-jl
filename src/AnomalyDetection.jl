module AnomalyDetection

include("SlidingWindows.jl"); using .SlidingWindows
include("DataHelper.jl"); using .DataHelper
include("SubstitutionStrategy.jl"); using .SubstitutionStrategies
include("DMMs.jl"); using .DMMs

end
