using CSV
using DataFrames
using Dates
using JSON

"""
    nyc_data_load()

Load New York taxi dataset
"""
function nyc_data_load()
	nyc_taxi_data_file = joinpath("..", "data", "nyc_taxi", "nyc_taxi.csv")
	nyc_taxi_labels_file = joinpath("..", "data", "nyc_taxi", "nyc_labels.json")

	# load the labels
	# converts string datetime to Dates.DateTime
	nyc_taxi_label = begin
		txt_labels = read(nyc_taxi_labels_file, String) |> JSON.parse
		labels = Dict()
		labels["point"] = map(txt_labels["point_label"]) do dt
			DateTime(dt, dateformat"yyyy-mm-dd HH:MM:SS.s")
		end
		labels["range"] = map(txt_labels["range_label"]) do dtrange
			@. DateTime(dtrange, dateformat"yyyy-mm-dd HH:MM:SS.s")
		end
		labels
	end

	# extract nyc taxi data from csv
	nyc_taxi_data = begin
		txt_df = CSV.File(nyc_taxi_data_file) |> DataFrame!

		# convert to Dates.DateTime
		realdates = map(txt_df.timestamp) do dt
			DateTime(dt, dateformat"yyyy-mm-dd HH:MM:SS.s")
		end

		# Vectorize the labels
		labels = map(realdates) do dt
			isanomally = dt ∈ nyc_taxi_label["point"] || 
			any(map(nyc_taxi_label["range"]) do dtrange
					 dt >= first(dtrange) &&
					 dt <= last(dtrange)
				 end)
		end

		# complete DataFrame
		DataFrame(timestamp = realdates, value = txt_df.value, label = labels)
	end

	return nyc_taxi_data
end
